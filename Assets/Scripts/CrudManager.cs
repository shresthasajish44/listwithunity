using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrudManager : MonoBehaviour
{

    public GameObject itemParent, players, input, update;

    // Start is called before the first frame update
    void Start()
    {
        Read();
    }

    public void Read() 
    {
        int count = PlayerPrefs.GetInt("count");

        for (int i = 0; i < itemParent.transform.childCount; i++) {
            Destroy(itemParent.transform.GetChild(i).gameObject);
        }

        int number = 0;
        for (int i = 0; i <= count; i++) {
            number++;
            string id = PlayerPrefs.GetString("id[" + i + "]");

            string name = PlayerPrefs.GetString("name[" + i + "]");
            string health = PlayerPrefs.GetString("health[" + i +"]");
            string level = PlayerPrefs.GetString("level[" + i +"]");

            if (id != "")
            {
                GameObject temp_item = Instantiate(players, itemParent.transform);
                temp_item.name = i.ToString();
                temp_item.transform.GetChild(0).GetComponent<Text>().text = number.ToString();
                temp_item.transform.GetChild(1).GetComponent<Text>().text = name;
                temp_item.transform.GetChild(2).GetComponent<Text>().text = health;
                temp_item.transform.GetChild(3).GetComponent<Text>().text = level;
            }
            else {
                number--;
            }
        }

    }

    public void Create() {
        int count = PlayerPrefs.GetInt("count");
        count++;
        PlayerPrefs.SetString("id[" + count + "]", count.ToString());
        PlayerPrefs.SetString("name[" + count + ']', input.transform.GetChild(1).GetComponent<InputField>().text);
        PlayerPrefs.SetString("health[" + count + ']', input.transform.GetChild(2).GetComponent<InputField>().text);
        PlayerPrefs.SetString("level[" + count + ']', input.transform.GetChild(3).GetComponent<InputField>().text);
        PlayerPrefs.SetInt("count", count);
        input.transform.GetChild(1).GetComponent<InputField>().text = "";
        input.transform.GetChild(2).GetComponent<InputField>().text = "";
        input.transform.GetChild(3).GetComponent<InputField>().text = "";
        Read();
    }

    public void Delete(GameObject players) {
        string id_pref = players.name;
        PlayerPrefs.DeleteKey("id[" + id_pref + "]");
        PlayerPrefs.DeleteKey("name[" + id_pref +"]");
        PlayerPrefs.DeleteKey("health[" + id_pref + "]");
        PlayerPrefs.DeleteKey("level[" + id_pref + "]");
        Read();
    }

    string id_edit;

    public void ListUpdate(GameObject obj_edit)
    {
        update.SetActive(true);
        id_edit = PlayerPrefs.GetString("id[" + obj_edit.name + "]");
        update.transform.GetChild(1).GetComponent<InputField>().text = PlayerPrefs.GetString("name["+ obj_edit.name +"]");
        update.transform.GetChild(2).GetComponent<InputField>().text = PlayerPrefs.GetString("health["+obj_edit.name+"]");
        update.transform.GetChild(3).GetComponent<InputField>().text = PlayerPrefs.GetString("level["+obj_edit.name+"]");
    }
    
    // Update is called once per frame
    public void EditedList()
    {
        PlayerPrefs.SetString("name[" + id_edit + "]", update.transform.GetChild(1).GetComponent<InputField>().text);
        PlayerPrefs.SetString("health[" + id_edit + "]", update.transform.GetChild(2).GetComponent<InputField>().text);
        PlayerPrefs.SetString("level[" + id_edit + "]", update.transform.GetChild(3).GetComponent<InputField>().text);
        Read();

    }

    private void Update()
    {
        
    }
}
