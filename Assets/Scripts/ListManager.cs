using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ListManager : MonoBehaviour
{
    List<Players> players = new();
    GameObject iListObject;
    //TMP_Text iList;
    GameObject iListName;
    TMP_InputField iName;
    GameObject iListLevel;
    TMP_InputField iLevel;
    GameObject iListHealth;
    TMP_InputField iHealth;

    public GameObject content;

    public GameObject ListObject;
    TMP_Text viewList;


    void Start()
    {
       // iListObject = GameObject.FindGameObjectWithTag("NameList");
        //iList = iListObject.GetComponent<TMP_Text>();

        iListName = GameObject.FindGameObjectWithTag("Name");
        iName = iListName.GetComponent<TMP_InputField>();
        
        iListLevel = GameObject.FindGameObjectWithTag("Level");
        iLevel = iListLevel.GetComponent<TMP_InputField>();

        iListHealth = GameObject.FindGameObjectWithTag("Health");
        iHealth = iListHealth.GetComponent<TMP_InputField>();

    }

    public void ButtonUpdate() {
        GameObject addedList = Instantiate(ListObject);
        addedList.transform.parent = content.transform;
        viewList = addedList.GetComponent<TMP_Text>();


        players.Add(new Players(iName.text, iLevel.text, iHealth.text));


        foreach (Players player in players)
        {


            viewList.text = ("Name:" + player._name + " Level:" + player._level + " Health:" + player._health);

            Debug.Log(player._name + " " + player._level + " " + player._health);
            //iList.text = ("Name:" + player._name + " Level:" + player._level + " Health:" + player._health);
        }

    }

    
    void Update()
    {
        //foreach (Players player in players)
        //{
            

         //   viewList.text = ("Name:" + player._name + " Level:" + player._level + " Health:" + player._health);

        //    Debug.Log(player._name + " " + player._level + " " + player._health);
            //iList.text = ("Name:" + player._name + " Level:" + player._level + " Health:" + player._health);
        //}
    }
}
